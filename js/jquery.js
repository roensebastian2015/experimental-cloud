$( function() {



    //This is a listener that listens when the document is ready to use..
    //IN other words have we downloaded everything is ready to go...
    $( document ).ready(function() {

      //Initialize all tool tips...
      $('[data-toggle="tooltip"]').tooltip();

      //Header display slow...
      $(".attTable").show();

      //this function makes the address ticket draggable...
      $("#ticketForm").draggable();
      //console.log($("#ticketForm"));

      // First we create tabs...
      // I hidden the header display to none...
      $( "#tabs" ).tabs({ hide: 'fadeOut', show: 'fadeIn' });
      $( ".selector" ).tabs({
      active: 1
    });

      $("#tabs").fadeIn("slow");
  });


} );
