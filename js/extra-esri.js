//Esri Map Service
app.factory('apiMap',  function($http){
  var apiMap = {
    map    : null,
    setMap : function(map){
      this.map = map;
    },
    getMap : function(){
      return this.map;
    }

  }
  return apiMap;
});

//Here I controll any type of layers needed....
app.factory('apiLayers', function($http, esriLoader) {
  var apiLayer = {
    addressLayer   : null,
    url            : ["../arcgis/rest/services/education/public_education/FeatureServer/0", "../php/spartan/api/index.php/layers/getBoxAddressPoints/"],
    getSallyFLayer : function(){
      var self    = this;
      var promise = esriLoader.require("esri/layers/FeatureLayer").then(function(FeatureLayer){

          return new FeatureLayer(self.url[0], {mode: FeatureLayer.MODE_ONDEMAND});
      });
      return promise;
    },
    getGAddressPointsLayer: function(){
      var self = this;
      var promise = esriLoader.require("esri/layers/GraphicsLayer").then(function(GraphicsLayer){
          self.addressLayer =new GraphicsLayer({id: "addresspoints"});
          return self.addressLayer;
      });
      return promise;
    },
    getBoxPoints:  function(obj){
      var promise = $http.post(this.url[1], {data: obj}).success(function(json){
             return json;
      });
      return promise;
    },

  };

  return apiLayer;
});

//Factory service for geometry....
app.factory('apiGeometry', function(esriLoader){
  var apiGeom = {
      webMercatorUtils: null,
      point           : null,
      setupWebUtils: function(){
        var promise = esriLoader.require("esri/geometry/webMercatorUtils").then(function(webMercatorUtils){
            return webMercatorUtils;
        });
        return promise;
      },
      setupPoint : function(){
        var self = this;
        var promise = esriLoader.require("esri/geometry/Point").then(function(Point){
            self.point = Point;
        });
        return promise;
      },
      setWebUtils: function(webUtils){
         this.webMercatorUtils = webUtils;
      }
  };
  return apiGeom;
});

//Factory service for symbology...
app.factory('apiSymbology', function(esriLoader){
    var apiSymbol = {
        textSymbol : null,
        textFont   : null,
        graphic    : null,
        setupTextSymbol: function(){
          var self    = this;
          var promise = esriLoader.require(["esri/symbols/TextSymbol","esri/Color", "esri/symbols/Font", "esri/graphic"]).then(function(modules){
            self.textSymbol = new modules[0];
            self.textSymbol.setColor(new modules[1]([110,197,233,0.7]));
            self.textSymbol.setHaloSize(2);
            self.textSymbol.setHaloColor(new modules[1]([0,0,0]));
            self.textFont  = new modules[2];
            self.textFont.setSize("14pt");
            self.textFont.setWeight(modules[2].WEIGHT_BOLD);
            self.textSymbol.setFont(self.textFont);
            self.graphic  = modules[3];
          });
          return promise;
        },
    };
   return apiSymbol;
});
