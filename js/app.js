var app = angular.module("spartanpro", ['esri.map']);


//Create Factory for our http api services....

app.factory('apiServices', function($http) {
  var apiService = {

    getInventory:     function(){
      var promise = $http.get(spartanpro.apiUrl + "inventory/getSupplies/").then(function(response){
          return response.data;
      });
      return promise;
    },
    getTotalSupplies: function() {
      // $http returns a promise, which has a then function, which also returns a promise
      var promise = $http.get(spartanpro.apiUrl + 'inventory/totalSupplies/').then(function (response) {

        return response.data;
      });
      // Return the promise to the controller
      return promise;
    },
    getBetweenDatesEvents: function(json){
      var promise = $http.post(spartanpro.apiUrl + "events/getBetweenDates/", {data: json}).success(function(response){
        return response;
      });
      return promise;
    },
    getEventCountByQtr: function(json)
    {
      var promise = $http.post('../php/education/api/index.php/events/getBetweenDatesCount/', {data: json}).then(function(response){
          return response.data;
      });
      return promise;
    },
    getDistruByEventId: function(eventid)
    {
      var promise = $http.post('../php/education/api/index.php/inventory/getDistru', {data: eventid}).success(function(response){
          return response;
      });
      return promise;
    }
  };
  return apiService;
});

//Create Our Factory for sharing scopes with other controllers...
app.factory('Scopes', function($rootScope){
  var mem = {};
  return {
    store: function(key, value){
          mem[key] = value;
    },
    get:   function(key){
        return mem[key];
  }
  };
});
