app.controller('map', function($scope, $http, $location,Scopes, apiServices){

  //Lets create a Scope global from this controller so we can use in other controllers...
  //Ex.. Such as variables or Functions...
  Scopes.store("map",$scope);

  //Private Variables..
  $scope.cntrlMpAn   = false;
  $scope.resizeDelay = 100;

  //Setup map configuration...
  $scope.option = {
            options: {
                force3DTransforms: true,
                navigationMode: 'css-transforms',
                basemap: 'topo-vector',
                center: [-98.0408394, 26.399803],
                zoom: 9,
            }
  };


  //Start Of Click Functions...
  $scope.displayTable = function(){

    if($scope.cntrlMpAn){
      $scope.cntrlMpAn = false;
      $("#map").animate({height:'100%'}, 300);
      $(".attTableDisplay").animate({bottom: "-14"}, 300);
      console.log($(".attTable").height());
    }
    else{
      $scope.cntrlMpAn = true;
      var mph = $("#map").height() / 2;


      $("#map").animate({height: mph}, 200);
      $(".attTableDisplay").animate({bottom: mph - 140}, 400);
    }

    // $(".attTable").animate({height:'40%'});

  }

  //Start to listener for map events...
  $scope.mapLoaded = function(map){
    $scope.point = map.extent.getCenter();

    //Lets get the map instance..
    $scope.map = map;

    if($scope.cntrlMpAn){
      var mph = $("#map").height() / 2;
      //Lets make sure that icon table display in the right location..
      //$(".attTableDisplay").animate({bottom: mph - 440}, 400);
    }


    //Resize events
    $scope.map.on('resize', function(evt){
        $scope.map.__resizeCenter = $scope.map.extent.getCenter();
        setTimeout(function(){
          map.centerAt($scope.map.__resizeCenter);
        }, $scope.resizeDelay);
    });

  }


});
