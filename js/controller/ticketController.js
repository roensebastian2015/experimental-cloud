app.controller('ticket', function($scope, $http,$location,Scopes, apiServices){
  //Lets Store our scope so we can use in other controllers...
  Scopes.store("ticket", $scope);


  //Setup Some Fancy functions for previous, next, Minify, Full Screen, and Close

  $scope.previous  = function(){

  };

  $scope.next      = function(){

  };

  $scope.minify    = function(){


  };

  $scope.fullScreen = function(){

  };

  $scope.close      = function(){
    $("#ticketForm").fadeOut("slow");
  };
});
